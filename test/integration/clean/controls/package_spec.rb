# frozen_string_literal: true

control 'rofi-pass-package-clean-pkg-removed' do
  title 'should not be installed'

  describe package('rofi-pass') do
    it { should_not be_installed }
  end
end
