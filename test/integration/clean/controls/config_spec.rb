# frozen_string_literal: true

control 'rofi-pass-config-file-rofi-pass-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/rofi-pass/config') do
    it { should_not exist }
  end
end

control 'rofi-pass-config-file-rofi-pass-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/rofi-pass') do
    it { should_not exist }
  end
end
