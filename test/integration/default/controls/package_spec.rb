# frozen_string_literal: true

control 'rofi-pass-package-install-pkg-installed' do
  title 'should be installed'

  describe package('rofi-pass') do
    it { should be_installed }
  end
end
