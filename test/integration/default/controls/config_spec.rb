# frozen_string_literal: true

control 'rofi-pass-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'rofi-pass-config-file-rofi-pass-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/rofi-pass') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'rofi-pass-config-file-rofi-pass-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/rofi-pass/config') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('/* Your changes will be overwritten.') }
    its('content') { should include("EDITOR='nvim-qt'") }
  end
end
