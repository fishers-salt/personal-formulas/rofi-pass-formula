# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as rofi_pass with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('rofi-pass-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_rofi-pass', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

rofi-pass-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

rofi-pass-config-file-rofi-pass-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/rofi-pass
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - rofi-pass-config-file-user-{{ name }}-present

rofi-pass-config-file-rofi-pass-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/rofi-pass/config
    - source: {{ files_switch([
                  name ~ '-config.tmpl',
                  'config.tmpl'],
                lookup='rofi-pass-config-file-rofi-pass-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - rofi-pass-config-file-rofi-pass-dir-{{ name }}-managed

{% endif %}
{% endfor %}
{% endif %}
