# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as rofi_pass with context %}

rofi-pass-package-install-pkgs-installed:
  pkg.installed:
    - name: {{ rofi_pass.pkg.name }}
